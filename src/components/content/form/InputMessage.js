import { Component, Fragment } from "react";

class InputMessage extends Component {
    inputChangeHandler = (event) => {
        let value = event.target.value;
        
        this.props.inputMessageChangeHandlerProps(value);
    }
    
    buttonClickHandler = () => {
        this.props.outputMessageChangeHandlerProps();
    }

    render() {
        return (
            <>
                <div className="form-floating mt-5">
                    <input type="email" className="form-control" placeholder="Nhập message của bạn" onChange={this.inputChangeHandler} value={this.props.inputMessageProps} />
                    <label>Message cho bạn 12 tháng tới</label>
                </div>
            
                <button className="btn btn-success m-5" onClick={this.buttonClickHandler}>Gửi thông điệp</button>
            </>
        )
    }
}

export default InputMessage;