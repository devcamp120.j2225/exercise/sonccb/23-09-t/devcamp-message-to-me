import { Component } from "react";
import InputMessage from "./form/InputMessage";
import LikeImage from "./result/LikeImage";

class ContentComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }

    inputMessageChangeHandler = (value) => {
        this.setState({
            inputMessage: value
        })
    }

    outputMessageChangeHandler = () => {
        if(this.state.inputMessage) {
            this.setState({
                outputMessage: [...this.state.outputMessage, this.state.inputMessage],
                likeDisplay: true
            })
        }
    }

    render() {
        return (
            <>
                <InputMessage inputMessageProps={this.state.inputMessage} inputMessageChangeHandlerProps={this.inputMessageChangeHandler} outputMessageChangeHandlerProps={this.outputMessageChangeHandler} />
                <LikeImage outputMessageProps={this.state.outputMessage} likeDisplayProps={this.state.likeDisplay} />
            </>
        )
    }
}

export default ContentComponent;