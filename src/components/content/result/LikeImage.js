import { Component } from "react";
import like from "../../../assets/images/like-yellow-icon.png";

class LikeImage extends Component {
    render() {
        let { outputMessageProps, likeDisplayProps } = this.props;

        return (
            <>  
                {outputMessageProps.map((element, index) => {
                    return   <p key={index}>{element}</p>
                })}
              

                { likeDisplayProps ?  <img src={like} alt="like" width="100"></img> : null } 
            </>
        )
    }
}

export default LikeImage;