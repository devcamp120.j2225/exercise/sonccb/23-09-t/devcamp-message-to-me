import { Component } from "react";
import background from '../../../assets/images/programing.png'

class TittleImage extends Component {
 render(){
  return(
   <img className='img-thumbnail' src={background} alt='title background'/>
  ); 
 }
}

export default TittleImage;