import { Component } from "react";
import TittleImage from "./text/TittleImage";
import TittleText from "./text/TittleText";

class TitleComponent extends Component {
 render(){
  return(
   <>
    <TittleText/>
    <TittleImage/>
   </>
  ); 
 }
}

export default TitleComponent;